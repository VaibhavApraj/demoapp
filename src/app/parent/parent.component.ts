import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {
@ViewChild('inputType') InputType:ElementRef | undefined;
inputTypeValue:any;
goToParent1:any;
  constructor() { }

  ngOnInit(): void {
  }
  goToParent(event:any)
  {
    console.log(event);
   this.goToParent1=event; 
  }
  goToChild(inputType: any)
  {
    this.inputTypeValue=inputType.value;
    console.log("inputType",inputType.value);
  }
  
}
