import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutesNavigateComponent } from './routes-navigate.component';

describe('RoutesNavigateComponent', () => {
  let component: RoutesNavigateComponent;
  let fixture: ComponentFixture<RoutesNavigateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoutesNavigateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutesNavigateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
