import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BickComponent } from './bick.component';

describe('BickComponent', () => {
  let component: BickComponent;
  let fixture: ComponentFixture<BickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
