import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-child-routes',
  templateUrl: './child-routes.component.html',
  styleUrls: ['./child-routes.component.css']
})
export class ChildRoutesComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  onfnclick()
  {
    console.log("1");
    
    this.router.navigate(['/routes-navigate']);
  }
}
