import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
@Input() inputTypeValue: any;
nimap:any;
@Output() asd=new EventEmitter<string>();
  constructor() { }

  ngOnInit(): void {
  }
  goToParent()
  {
    this.asd.emit('NimapInfotech');
  }
}
