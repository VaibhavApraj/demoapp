import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-structural-directives',
  templateUrl: './structural-directives.component.html',
  styleUrls: ['./structural-directives.component.css'],
})
export class StructuralDirectivesComponent implements OnInit {
  @ViewChild('addArrayDataInput') searchValue!: ElementRef;
  isClick: boolean = false;
  isClick1: boolean = false;
  selectedData: string = '';
  addName: any = [];
  dataAll = [
    {
      name: "vaibhav", salary: 2000, post: 'developer'
    },
    {
      name: "sanjay", salary: 3000, post: 'tester'
    },
    {
      name: "apraj", salary: 5000, post: 'uiux'
    },
    {
      name: "shital", salary: 7000, post: 'developer'
    },
    {
      name: "ajay", salary: 2000, post: 'tester'
    },
    {
      name: "rahul", salary: 6000, post: 'developer'
    },
    {
      name: "sushil", salary: 2000, post: 'manager'
    }
  ];
  constructor() { }

  ngOnInit(): void { }
  ifCLick() {
    this.isClick = true;
  }
  ifCLick1() {
    this.isClick1 = true;
  }
  onSwitch(event: any) {
    console.log(event.target.value);
    this.selectedData = event.target.value
  }
  addData(name1: any) {
    this.addName.push({ name: name1.value });
    this.searchValue.nativeElement.value = '';
  }
  deleteData(i: any) {
    this.addName.splice(i, 1)
  }
}
