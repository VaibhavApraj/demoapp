import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DataBindingComponent } from './data-binding/data-binding.component';
import { StructuralDirectivesComponent } from './structural-directives/structural-directives.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RoutesNavigateComponent } from './routes-navigate/routes-navigate.component';
import { CarComponent } from './routes-navigate/car/car.component';
import { BickComponent } from './routes-navigate/bick/bick.component';
import { HouseComponent } from './routes-navigate/house/house.component';
import { WatchComponent } from './routes-navigate/watch/watch.component';
import { MobileComponent } from './routes-navigate/mobile/mobile.component';
import { LaptopComponent } from './routes-navigate/laptop/laptop.component';
import { ChildRoutesComponent } from './routes-navigate/child-routes/child-routes.component';
import { NestedRoutesComponent } from './routes-navigate/nested-routes/nested-routes.component';
import { NgContentComponent } from './ng-content/ng-content.component';
import { CardsComponent } from './ng-content/cards/cards.component';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './child/child.component';
import { Exercise1Component } from './exercise1/exercise1.component';
import { ChildCardComponent } from './exercise1/child-card/child-card.component';
import { SubjectBehaviorComponent } from './subject-behavior/subject-behavior.component';

@NgModule({
  declarations: [
    AppComponent,
    DataBindingComponent,
    StructuralDirectivesComponent,
    RoutesNavigateComponent,
    CarComponent,
    BickComponent,
    HouseComponent,
    WatchComponent,
    MobileComponent,
    LaptopComponent,
    ChildRoutesComponent,
    NestedRoutesComponent,
    NgContentComponent,
    CardsComponent,
    ParentComponent,
    ChildComponent,
    Exercise1Component,
    ChildCardComponent,
    SubjectBehaviorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
