import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DataBindingComponent } from './data-binding/data-binding.component';
import { Exercise1Component } from './exercise1/exercise1.component';
import { NgContentComponent } from './ng-content/ng-content.component';
import { ParentComponent } from './parent/parent.component';
import { BickComponent } from './routes-navigate/bick/bick.component';
import { CarComponent } from './routes-navigate/car/car.component';
import { HouseComponent } from './routes-navigate/house/house.component';
import { LaptopComponent } from './routes-navigate/laptop/laptop.component';
import { MobileComponent } from './routes-navigate/mobile/mobile.component';
import { NestedRoutesComponent } from './routes-navigate/nested-routes/nested-routes.component';
import { RoutesNavigateComponent } from './routes-navigate/routes-navigate.component';
import { WatchComponent } from './routes-navigate/watch/watch.component';
import { StructuralDirectivesComponent } from './structural-directives/structural-directives.component';
import { SubjectBehaviorComponent } from './subject-behavior/subject-behavior.component';

const routes: Routes = [
  {
    path:'',
    redirectTo:"/data-binding",
    pathMatch:'full'
  },
  {
    path:'data-binding',
    component:DataBindingComponent
  },
  {
    path:'structure-directives',
    component:StructuralDirectivesComponent
  },
  {
    path:'routes-navigate',
    // component:NestedRoutesComponent,
    // **--for nested routes
    children:[
     { path:'',component:RoutesNavigateComponent},
     // **--for componestes redirection
     { path:'car',component:CarComponent},
     { path:'mobile',component:MobileComponent},
     { path:'laptop',component:LaptopComponent},
     { path:'house',component:HouseComponent},
     { path:'watch',component:WatchComponent},
     { path:'bick',component:BickComponent},
     { path:'nested-routes',component:NestedRoutesComponent,
    children:[
     { path:'car',component:CarComponent},
     { path:'mobile',component:MobileComponent},
     { path:'laptop',component:LaptopComponent},
     { path:'house',component:HouseComponent},
     { path:'watch',component:WatchComponent},
     { path:'bick',component:BickComponent},
    ]},
     { path:'child-routes',component:RoutesNavigateComponent},
    ],
  },
  {
    path:'ng-content',
    component:NgContentComponent
  },
  {
    path:'parent',
    component:ParentComponent
  },
  {
    path:'exercise1',
    component:Exercise1Component
  },
  {
    path:'subject-behavior',
    component:SubjectBehaviorComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
