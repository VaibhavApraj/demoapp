import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.css']
})
export class DataBindingComponent implements OnInit {

  interpolation:string="Vaibhav";
  myStatus:boolean=true;
  font='50px';
  green:boolean=true;
  referenceVariable:any;
  binding:string='two way databinding';
  constructor() { }

  ngOnInit(): void {
  }

  myFunction(event: any) {
    console.log('Hello World',event);
  }
  ref(refVariable: any)
  {
    this.referenceVariable=refVariable.value
  }
}
