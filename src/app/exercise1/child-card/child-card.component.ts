import { Component, OnInit, Output ,EventEmitter, ViewChild, ElementRef, Input} from '@angular/core';

@Component({
  selector: 'app-child-card',
  templateUrl: './child-card.component.html',
  styleUrls: ['./child-card.component.css']
})
export class ChildCardComponent implements OnInit {
  inputName:any;
  @Input() cardStyle: any;
  @Input() cardPlaceholder:any;
  @Output() dataPassing = new EventEmitter();
  @ViewChild('inputData') clearData !: ElementRef ;
  totalCount:number=0;
  constructor() { }

  ngOnInit(): void {
    console.log("cardStyle",this.cardStyle)
  }
  expression(data:any)
  {
    console.log(data);
    this.dataPassing.emit(data);
    this.clearData.nativeElement.value='';
    this.totalCount+=1;
  }
}
