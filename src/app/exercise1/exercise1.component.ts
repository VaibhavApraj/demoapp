import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-exercise1',
  templateUrl: './exercise1.component.html',
  styleUrls: ['./exercise1.component.css']
})
export class Exercise1Component implements OnInit {

  adminData:any=[];
  userData:any=[];
  constructor() { }

  ngOnInit(): void {
  }
  userDataFun(event:any)
  {
    this.userData.push(event);
  }
  deleteUser(indexData:any)
  {
    this.userData.splice(indexData,1);
  }
  
  adminDataFun(event:any)
  {
    this.adminData.push(event);
  }
  deleteAdmin(indexData:any)
  {
    this.adminData.splice(indexData,1);
  }
}
